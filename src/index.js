'use strict';

import { wait, ready, keychain } from './methods';
import { resolveWithValue } from './helpers';

export default ({ patchSetters }) => ({ proto }) => {

	if (patchSetters) {
		proto.set = resolveWithValue.call(proto, proto.set);
		proto.push = resolveWithValue.call(proto, proto.push);
		proto.unshift = resolveWithValue.call(proto, proto.unshift);
		proto.add = resolveWithValue.call(proto, proto.add);
		proto.subtract = resolveWithValue.call(proto, proto.subtract);
		proto.reset = resolveWithValue.call(proto, proto.reset);
	}

	proto.wait = wait;	
	proto.ready = ready;
	proto.keychain = keychain;
};